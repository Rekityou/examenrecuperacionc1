package com.example.examenrecuperacion;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class ConversionActivity extends AppCompatActivity {

    private EditText editTemp;
    private TextView txtConversionResult, txtWelcome;
    private Button btnConvertTemp, btnClear, btnBack;
    private RadioButton rbCelsiusToFahrenheit, rbFahrenheitToCelsius;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversion);

        editTemp = findViewById(R.id.editTemp);
        txtConversionResult = findViewById(R.id.txtConversionResult);
        txtWelcome = findViewById(R.id.txtWelcome);
        btnConvertTemp = findViewById(R.id.btnConvertTemp);
        btnClear = findViewById(R.id.btnClear);
        btnBack = findViewById(R.id.btnBack);
        rbCelsiusToFahrenheit = findViewById(R.id.rbCelsiusToFahrenheit);
        rbFahrenheitToCelsius = findViewById(R.id.rbFahrenheitToCelsius);

        String name = getIntent().getStringExtra("name");
        txtWelcome.setText("Bienvenido " + name);

        btnConvertTemp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convertTemperature();
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTemp.setText("");
                txtConversionResult.setText(getString(R.string.conversion_result));
                rbCelsiusToFahrenheit.setChecked(false);
                rbFahrenheitToCelsius.setChecked(false);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void convertTemperature() {
        String tempStr = editTemp.getText().toString();

        if (TextUtils.isEmpty(tempStr)) {
            Toast.makeText(this, "Por favor, ingresa una temperatura", Toast.LENGTH_SHORT).show();
            return;
        }

        float temp = Float.parseFloat(tempStr);
        float result;

        if (rbCelsiusToFahrenheit.isChecked()) {
            result = (temp * 9/5) + 32;
            txtConversionResult.setText(String.format("Resultado: %.2f °F", result));
        } else if (rbFahrenheitToCelsius.isChecked()) {
            result = (temp - 32) * 5/9;
            txtConversionResult.setText(String.format("Resultado: %.2f °C", result));
        } else {
            Toast.makeText(this, "Por favor, selecciona una conversión", Toast.LENGTH_SHORT).show();
        }
    }
}
